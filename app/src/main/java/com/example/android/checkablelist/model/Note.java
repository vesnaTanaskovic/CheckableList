package com.example.android.checkablelist.model;

/**
 * Created by Tanaskovic on 1/9/2018.
 */

public class Note {

    private String note;
    private boolean done;

    public Note(){

    }

    public Note(String note, boolean done) {
        this.note = note;
        this.done = done;
    }

    public boolean isDone() {

        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
