package com.example.android.checkablelist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.android.checkablelist.R;
import com.example.android.checkablelist.model.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tanaskovic on 1/9/2018.
 */

public class NotesAdapter extends BaseAdapter {

    private List<Note> notes = new ArrayList<>();

    private final Context context;

    private final LayoutInflater layoutInflater;

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public NotesAdapter(Context context, List<Note> notes) {
        this.context = context;
        this.notes = notes;
        this.layoutInflater = LayoutInflater.from(context);

    }

    private static class ViewHolder{

        private TextView textView;
        private CheckBox checkBox;
    }
    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        final ViewHolder viewHolder;

        if (view == null) {
            // create a new view holder since we don't have a view to reuse
            viewHolder = new ViewHolder();

            // inflate view item
            view = layoutInflater.inflate(R.layout.view_item_note, viewGroup, false);

            // store the views in a view holder for later reuse
            viewHolder.checkBox = (CheckBox) view.findViewById(R.id.done);
            viewHolder.textView = (TextView) view.findViewById(R.id.text);

            // save view holder for later reuse
            view.setTag(viewHolder);
        } else {
            // we already have a view holder
            viewHolder = (ViewHolder) view.getTag();
        }

        // get item
        final Note note = (Note) getItem(position);

        // populate view item with task's data
        viewHolder.checkBox.setChecked(note.isDone());
        viewHolder.textView.setText(note.getNote());

        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Note currentNote= notes.get(position);

                // save the current state to task, needed because of cell reuse
                currentNote.setDone(viewHolder.checkBox.isChecked());
                notes.set(position, currentNote);

            }
        });

        return view;
    }
}
