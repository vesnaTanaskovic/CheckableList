package com.example.android.checkablelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.android.checkablelist.adapter.NotesAdapter;
import com.example.android.checkablelist.model.Note;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    private NotesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    /**
     * Initialize list view
     */
    private void init() {
        // get listView's reference
        listView = (ListView) findViewById(R.id.listView);

        // initialize items
        final List<Note> noteList = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            noteList.add(new Note("Note" + i, i % 2 == 0));
        }

        // initialize adapter
        adapter = new NotesAdapter(this, noteList);

        // set adapter to list
        listView.setAdapter(adapter);
    }
}
